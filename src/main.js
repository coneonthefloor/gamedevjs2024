import kaboom from "kaboom";

const PLAYER_SPEED = 300;
const WIDTH = 800;
const HEIGHT = 600;
const FLOOR_HEIGHT = 80;
const GUTTER_WIDTH = 130;
const HORIZON = 155;

const k = kaboom({
    width: WIDTH,
    height: HEIGHT,
});

k.loadSprite("bean", "sprites/bean.png");
k.loadSprite("bg", "sprites/road-bg.png");
k.loadSprite("enemy", "sprites/enemy.png");
k.loadSprite("spawn-zone", "sprites/spawn-zone.png");

k.scene("game", () => {
    k.add([k.sprite("bg")]);

    const spawnZones = [
        k.add([sprite("spawn-zone"), k.pos(260, HORIZON), k.scale(20)]),
        k.add([sprite("spawn-zone"), k.pos(320, HORIZON), k.scale(20)]),
        k.add([sprite("spawn-zone"), k.pos(380, HORIZON), k.scale(20)]),
        k.add([sprite("spawn-zone"), k.pos(440, HORIZON), k.scale(20)]),
        k.add([sprite("spawn-zone"), k.pos(500, HORIZON), k.scale(20)]),
    ];

    const enemyTargets = [
        k.add([
            "target",
            sprite("spawn-zone"),
            k.body(),
            k.area(),
            k.pos(100, HEIGHT + 200),
            k.scale(20),
        ]),
        k.add([
            "target",
            sprite("spawn-zone"),
            k.body(),
            k.area(),
            k.pos(200, HEIGHT + 200),
            k.scale(20),
        ]),
        k.add([
            "target",
            sprite("spawn-zone"),
            k.body(),
            k.area(),
            k.pos(300, HEIGHT + 200),
            k.scale(20),
        ]),
        k.add([
            "target",
            sprite("spawn-zone"),
            k.body(),
            k.area(),
            k.pos(400, HEIGHT + 200),
            k.scale(20),
        ]),
        k.add([
            "target",
            sprite("spawn-zone"),
            k.body(),
            k.area(),
            k.pos(500, HEIGHT + 200),
            k.scale(20),
        ]),
        k.add([
            "target",
            sprite("spawn-zone"),
            k.body(),
            k.area(),
            k.pos(600, HEIGHT + 200),
            k.scale(20),
        ]),
        k.add([
            "target",
            sprite("spawn-zone"),
            k.body(),
            k.area(),
            k.pos(700, HEIGHT + 200),
            k.scale(20),
        ]),
    ];

    const player = k.add([
        "player",
        k.sprite("bean"),
        k.anchor("center"),
        k.pos(WIDTH / 2, HEIGHT - FLOOR_HEIGHT),
        k.body(),
        k.area(),
    ]);

    k.onKeyDown("left", () => {
        if (player.pos.x > GUTTER_WIDTH) {
            player.move(-PLAYER_SPEED, 0);
        }
    });

    k.onKeyDown("right", () => {
        if (player.pos.x < WIDTH - GUTTER_WIDTH) {
            player.move(PLAYER_SPEED, 0);
        }
    });

    k.loop(2.5, () => {
        const spawnZone = spawnZones[k.randi(spawnZones.length - 1)];
        const target = enemyTargets[k.randi(enemyTargets.length - 1)];
        k.add([
            "enemy",
            k.sprite("enemy"),
            k.anchor("center"),
            k.scale(20),
            k.body(),
            k.area(),
            k.pos(spawnZone.pos.x, spawnZone.pos.y),
            {
                target,
                maxScale: 50,
                speed: 10,
                maxSpeed: 100,
            },
        ]);
    });

    let t = 0;
    k.onUpdate("enemy", (enemy) => {
        enemy.moveTo(enemy.target.pos, enemy.speed);

        if (enemy.speed < enemy.maxSpeed) {
            enemy.speed += 1;
        }

        enemy.scale.x += 0.5;
        enemy.scale.y += 0.5;
        if (!t) {
            console.log(enemy.scale);
            t++;
        }
    });

    k.onCollide("enemy", "target", (enemy) => {
        k.destroy(enemy);
    });

    k.onCollide("enemy", "player", (enemy, player) => {
        k.destroy(enemy);
        k.addKaboom(player.pos);
        k.burp();
    });
});

k.go("game");
